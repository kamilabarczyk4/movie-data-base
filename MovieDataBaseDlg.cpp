
// MovieDataBaseDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "MovieDataBase.h"
#include "MovieDataBaseDlg.h"
#include "afxdialogex.h"
#include "CMovie.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMovieDataBaseDlg dialog


CMovieDataBaseDlg::CMovieDataBaseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MOVIEDATABASE_DIALOG, pParent)
	, fDirector(_T(""))
	, fTitle(_T(""))
	, fYear(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMovieDataBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_YEAR, fYear);
	DDX_Text(pDX, IDC_EDIT_DIRECTOR, fDirector);
	DDX_Text(pDX, IDC_EDIT_TITLE, fTitle);
	DDX_Control(pDX, IDC_COMBO_RATING, fRating);
	DDX_Control(pDX, IDC_COMBO_GENRE, fGenre);
	DDX_Control(pDX, IDC_LIST1, m_List);

	DDX_Control(pDX, IDC_COMBO_GENRE_FILTER, fGenreFilter);
}

BEGIN_MESSAGE_MAP(CMovieDataBaseDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ADD, &CMovieDataBaseDlg::OnClickedButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, &CMovieDataBaseDlg::OnBnClickedButtonRemove)
	ON_BN_CLICKED(IDC_BUTTON_SORT, &CMovieDataBaseDlg::OnBnClickedButtonSort)
	//ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CMovieDataBaseDlg::OnLvnItemchangedList1)
	ON_BN_CLICKED(IDC_BUTTON_FILTER, &CMovieDataBaseDlg::OnClickedButtonFilter)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CMovieDataBaseDlg::OnBnClickedButtonLoad)
	
END_MESSAGE_MAP()


// CMovieDataBaseDlg message handlers

BOOL CMovieDataBaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	// Add film genre to the combo box
	fGenre.AddString(_T("Comedy"));
	fGenre.AddString(_T("Action"));
	fGenre.AddString(_T("Family"));
	fGenre.AddString(_T("Fantasy"));
	fGenre.AddString(_T("Adventure"));
	fGenre.AddString(_T("Science fiction"));
	fGenre.AddString(_T("Horror"));
	fGenre.AddString(_T("Drama"));
	fGenre.AddString(_T("Documentary"));
	fGenre.AddString(_T("Crime"));
	fGenre.AddString(_T("Animated"));
	fGenre.AddString(_T("Historical"));
	fGenre.AddString(_T("Musical"));
	fGenre.AddString(_T("Thriller"));
	// Add rating to the combo box
	fRating.AddString(_T("1"));
	fRating.AddString(_T("2"));
	fRating.AddString(_T("3"));
	fRating.AddString(_T("4"));
	fRating.AddString(_T("5"));
	fRating.AddString(_T("6"));
	fRating.AddString(_T("7"));
	fRating.AddString(_T("8"));
	fRating.AddString(_T("9"));
	fRating.AddString(_T("10"));
	// Add columns to list control
	m_List.InsertColumn(0, _T("Title"));
	m_List.SetColumnWidth(0, 400);
	m_List.InsertColumn(1, _T("Director"));
	m_List.SetColumnWidth(1, 150);
	m_List.InsertColumn(2, _T("Year"));
	m_List.SetColumnWidth(2, 70);
	m_List.InsertColumn(3, _T("Genre"));
	m_List.SetColumnWidth(3, 100);
	m_List.InsertColumn(4, _T("Rating"));
	m_List.SetColumnWidth(4, 50);

	// Add film genre to the combo box
	fGenreFilter.AddString(_T("Comedy"));
	fGenreFilter.AddString(_T("Action"));
	fGenreFilter.AddString(_T("Family"));
	fGenreFilter.AddString(_T("Fantasy"));
	fGenreFilter.AddString(_T("Adventure"));
	fGenreFilter.AddString(_T("Science fiction"));
	fGenreFilter.AddString(_T("Horror"));
	fGenreFilter.AddString(_T("Drama"));
	fGenreFilter.AddString(_T("Documentary"));
	fGenreFilter.AddString(_T("Crime"));
	fGenreFilter.AddString(_T("Animated"));
	fGenreFilter.AddString(_T("Historical"));
	fGenreFilter.AddString(_T("Musical"));
	fGenreFilter.AddString(_T("Thriller"));
	// Create data base text file if not exist 
	CFile f;
	BOOL bOpenOK;

	CFileStatus status;
	if (!CFile::GetStatus(_T("DataBase.txt"), status))
	{
		// Open the file without the Create flag
		bOpenOK = f.Open(_T("DataBase.txt"),
			CFile::modeCreate);
		f.Close();
	}
	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMovieDataBaseDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMovieDataBaseDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMovieDataBaseDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMovieDataBaseDlg::UploadData(std::vector <CMovie> vec) {
	///////////////////////////////////////////
	/// This function displays data
	///////////////////////////////////////////
	/// INPUT: vec - vector movies to view
	///////////////////////////////////////////

	// Clear the displayed screen
	m_List.DeleteAllItems();

	// display elements from the vector one by one
	for (auto i : vec) {

		int nIndex = m_List.InsertItem(0, i.fTitle);
		m_List.SetItemText(nIndex, 1, i.fDirector);
		m_List.SetItemText(nIndex, 2, i.fYear);
		m_List.SetItemText(nIndex, 3, i.fGenre);
		m_List.SetItemText(nIndex, 4, i.fRating);
	}
}


void CMovieDataBaseDlg::OnClickedButtonAdd()
{	///////////////////////////////////////////////////////////////////
	//This function adds a new movie and updates the displayed data
	//////////////////////////////////////////////////////////////////
	CMovie c;

	UpdateData(TRUE);

	CString sValue;
	CStdioFile f;
	
	int indexG;
	int indexR;
	CString genre;
	CString rating;

	// Get string value from combobox
	indexG = fGenre.GetCurSel();
	fGenre.GetLBText(indexG, genre);
	indexR = fRating.GetCurSel();
	fRating.GetLBText(indexR, rating);
	
	// Open existing file and save new data
	if (f.Open(_T("DataBase.txt"), CFile::modeNoTruncate | CFile::modeWrite)) {

		f.SeekToEnd();
			f.WriteString(fTitle);
			f.WriteString(L"|");
			f.WriteString(fDirector);
			f.WriteString(L"|");
			f.WriteString(genre);
			f.WriteString(L"|");
			sValue.Format(_T("%d"), fYear);
			f.WriteString(sValue);
			f.WriteString(L"|");
			f.WriteString(rating);
			f.WriteString(L"\n");
		f.Close();
	}
	UpdateData(FALSE);

	// Updating the displayed data
	std::vector <CMovie> vec = c.ReadData();
	UploadData(vec);
}


void CMovieDataBaseDlg::OnBnClickedButtonRemove()
{	///////////////////////////////////////////////////////////////////////////
	/// This function remove a existing movie and updates the displayed data
	///////////////////////////////////////////////////////////////////////////
	
	CMovie c;

	std::vector <CMovie> vec = c.ReadData();

	UpdateData(TRUE);
	CStdioFile f;

	// Get the title of the movie to be deleted
	int nItem = m_List.GetSelectionMark();
	if (nItem != -1) {
		CString strDeleteTitle = m_List.GetItemText(nItem, 0);
		CString strDeleteDirector = m_List.GetItemText(nItem, 1);
	
	f.Open(_T("DataBaseTmp.txt"), CFile::modeCreate | CFile::modeWrite);

	// Save all data expect the data to be deleted
	for (auto v : vec) {

		if ((v.fTitle == strDeleteTitle) && (v.fDirector==strDeleteDirector))
			continue;
		else{
			f.SeekToEnd();
			f.WriteString(v.fTitle);
			f.WriteString(L"|");
			f.WriteString(v.fDirector);
			f.WriteString(L"|");
			f.WriteString(v.fGenre);
			f.WriteString(L"|");	
			f.WriteString(v.fYear);
			f.WriteString(L"|");
			f.WriteString(v.fRating);
			f.WriteString(L"\n");
		}
	}

	f.Close();
	// Replace the file with new data
	remove("DataBase.txt");
	rename("DataBaseTmp.txt", "DataBase.txt");
	}

	UpdateData(FALSE);

	// Updating the displayed data
	std::vector <CMovie> tempVector = c.ReadData();
	UploadData(tempVector);

}

// To set the column index
int colNumber;

// The ListCompareFunc() method is a global function used by SortItemEx().
// This method is for compare CString
int CALLBACK ListCompareFunc(
	LPARAM lParam1,
	LPARAM lParam2,
	LPARAM lParamSort)
{
	CListCtrl* pListCtrl = (CListCtrl*)lParamSort;
	CString    strItem1 = pListCtrl->GetItemText(static_cast<int>(lParam1), colNumber);
	CString    strItem2 = pListCtrl->GetItemText(static_cast<int>(lParam2), colNumber);
	int result = 0;
	
	strItem1.MakeLower();
	strItem2.MakeLower();
	if (strItem1 < strItem2)
	{
		result = -1;
	}
	
	else if (strItem1 == strItem2) 
		result = 0;
	else
		result = 1;

	return result;
}

// The ListCompareFuncInt() method is a global function used by SortItemEx().
// This method is for compare int
int CALLBACK ListCompareFuncInt(
	LPARAM lParam1,
	LPARAM lParam2,
	LPARAM lParamSort)
{
	CListCtrl* pListCtrl = (CListCtrl*)lParamSort;
	CString    strItem1 = pListCtrl->GetItemText(static_cast<int>(lParam1), colNumber);
	CString    strItem2 = pListCtrl->GetItemText(static_cast<int>(lParam2), colNumber);
	int x1 = _tstoi(strItem1.GetBuffer());
	int x2 = _tstoi(strItem2.GetBuffer());
	int result = 0;
	if ((x1 - x2) > 0)
		result = -1;
	else if ((x1 - x2) == 0)
		result = 0;
	else
		result = 1;

	return result;
}


void CMovieDataBaseDlg::OnBnClickedButtonSort()
{	////////////////////////////////////////////////
	// SortItemsEx
	// Sort by the value of individual columns
	////////////////////////////////////////////////

		CButton* poButton;
		// Sort by title alphabetically
		poButton = (CButton*)(GetDlgItem(IDC_RADIO_TITLE));
		if (poButton->GetCheck() == 1) {
			colNumber = 0;
			m_List.SortItemsEx(ListCompareFunc, (LPARAM)&m_List);
		}
		// Sort by director alphabetically
		poButton = (CButton*)(GetDlgItem(IDC_RADIO_DIRECTOR));
		if (poButton->GetCheck() == 1) {
			colNumber = 1;
			m_List.SortItemsEx(ListCompareFunc, (LPARAM)&m_List);
		}
		// Sort by year in ascending order
		poButton = (CButton*)(GetDlgItem(IDC_RADIO_YEAR));
		if (poButton->GetCheck() == 1) {
			colNumber = 2;
			m_List.SortItemsEx(ListCompareFuncInt, (LPARAM)&m_List);
		}
		// Sort by rating in ascending order
		poButton = (CButton*)(GetDlgItem(IDC_RADIO_RATING));
		if (poButton->GetCheck() == 1) {
			colNumber = 4;
			m_List.SortItemsEx(ListCompareFuncInt, (LPARAM)&m_List);
		}

}


void CMovieDataBaseDlg::OnLvnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here

	*pResult = 0;
}


void CMovieDataBaseDlg::OnBnClickedButtonLoad()
{	///////////////////////////////////////////////////
	// Loads data from the file to the list control
	///////////////////////////////////////////////////

	CMovie c;
	std::vector <CMovie> vec = c.ReadData();
	UploadData(vec);

}


void CMovieDataBaseDlg::OnClickedButtonFilter()
{	//////////////////////////////////////////////////////////////////
	// This function displays movies of the specified genre
	//////////////////////////////////////////////////////////////////

	int indexG;
	CString genre;
	// Get film genre
	indexG = fGenreFilter.GetCurSel();
	fGenreFilter.GetLBText(indexG, genre);
	// Read data from text file
	CMovie c;
	std::vector <CMovie> vec = c.ReadData();
	std::vector <CMovie> vecFiltered = {};

	// Get only specified items
	for (auto i : vec) {

		if (i.fGenre == genre) {
			vecFiltered.push_back(i);
		}
	}
	// Updload displayed data
	UploadData(vecFiltered);
}
