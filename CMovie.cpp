#include "pch.h"
#include "CMovie.h"
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "framework.h"


CMovie::CMovie() {

	fTitle = L"";
	fDirector = L"";
	fGenre = L"";
	fYear = L"";
	fRating = L"";
}



CMovie::CMovie(const CMovie& fMovie)
{
	fTitle = fMovie.fTitle;
	fDirector = fMovie.fDirector;
	fYear = fMovie.fYear;
	fGenre = fMovie.fGenre;
	fRating = fMovie.fRating;
}


std::vector< CMovie > CMovie::ReadData() {

	// Movie sorting function
	CMovie c;
	CStdioFile f;

	std::vector<CMovie> vec;

	CString tmp;
	
	if (f.Open(_T("DataBase.txt"), CFile::modeRead)) {
		while (!feof(f.m_pStream))
		{
			f.ReadString(tmp);
			
			int nTokenPos = 0;
			int i = 0;
			CString strToken = tmp.Tokenize(_T("|"), nTokenPos);
			

			while (!strToken.IsEmpty())
			{
				

				if (i == 0)
					c.fTitle = strToken;
				else if (i == 1)
					c.fDirector = strToken;
				else if (i == 2)
					c.fGenre = strToken;
				else if (i == 3)
					c.fYear = strToken;
				else if (i == 4)
					c.fRating = strToken;

				i++;

				if (i == 5) i = 0;

				strToken = tmp.Tokenize(_T("|"), nTokenPos);
			}
			if (tmp != "")
			vec.push_back(c);

			tmp.Empty();
		}
		f.Close();
	}

	return vec;
}

