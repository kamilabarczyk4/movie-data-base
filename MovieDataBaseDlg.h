
// MovieDataBaseDlg.h : header file
//
#include <vector>
#include "CMovie.h"
#pragma once


// CMovieDataBaseDlg dialog
class CMovieDataBaseDlg : public CDialogEx
{
// Construction
public:
	CMovieDataBaseDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MOVIEDATABASE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	

	int fYear;
	CString fDirector;
	CString fTitle;
	CComboBox fRating;
	CComboBox fGenre;
	CListCtrl m_List;

	afx_msg void OnClickedButtonAdd();
	afx_msg void OnBnClickedButtonRemove();
	afx_msg void OnBnClickedButtonSort();
	afx_msg void OnLvnItemchangedList1( NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButtonLoad();

	void UploadData(std::vector <CMovie> vec);

	afx_msg void OnClickedButtonFilter();
	CComboBox fGenreFilter;
};
