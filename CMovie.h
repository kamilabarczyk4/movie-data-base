#pragma once
#include <afx.h>
#include <iostream>
#include <vector>

class CMovie :
    public CObject
{
public: 
    CString fTitle;
    CString fDirector;
    CString fGenre;
    CString fYear;
    CString fRating;

    CMovie();
   
    CMovie::CMovie(const CMovie& fMovie);
    std::vector <CMovie> CMovie::ReadData();
    
 
};

