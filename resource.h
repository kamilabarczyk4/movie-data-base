//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MovieDataBase.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MOVIEDATABASE_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_ADD                  1000
#define IDC_EDIT_TITLE                  1001
#define IDC_EDIT_DIRECTOR               1002
#define IDC_EDIT_YEAR                   1003
#define IDC_COMBO_GENRE                 1004
#define IDC_COMBO_RATING                1005
#define IDC_BUTTON_REMOVE               1006
#define IDC_COMBO_GENRE2                1007
#define IDC_COMBO_GENRE_FILTER          1007
#define IDC_BUTTON_SORT                 1008
#define IDC_LIST1                       1009
#define IDC_BUTTON_LOAD                 1012
#define IDC_RADIO_TITLE                 1018
#define IDC_RADIO_DIRECTOR              1019
#define IDC_RADIO_YEAR                  1020
#define IDC_RADIO_RATING                1021
#define IDC_BUTTON_FILTER               1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
